import java.util.Random;
import java.util.concurrent.TimeUnit;

public class BinarySearchBenchmark {
	
	static int size = 400_000_000;
	static int[] ARRAY = new int[400_000_000];
	static int currentPos = 0;
	static int totalValues = 0;
	
	public static void main(String[] args) {

		Thread main = new Thread() {
			@Override
			public void run() {
				long testStart = System.currentTimeMillis();

				long arrayGenerateStart = System.currentTimeMillis();

				Thread worker1 = startWorker();				
				Thread worker2 = startWorker();				
				Thread worker3 = startWorker();
				Thread worker4 = startWorker();				
				Thread worker5 = startWorker();				
				Thread worker6 = startWorker();
				Thread worker7 = startWorker();
				
				try {
					worker1.join();
					worker2.join();
					worker3.join();
					worker4.join();
					worker5.join();
					worker6.join();
					worker7.join();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}

				long arrayGenEnd = System.currentTimeMillis();

				System.out.println("Random array generated! Total values: " + totalValues + ". It took: " + 
						TimeUnit.MILLISECONDS.toSeconds(arrayGenEnd - arrayGenerateStart) + " seconds");

				//Quick sort time
				long quickSortStart = System.currentTimeMillis();

				Thread sort = new Thread() {
					@Override
					public void run() {
						System.out.println("Starting Quick sort!");
						ThreadedQuick.quicksort(ARRAY);
					}
				};
				sort.setPriority(Thread.MAX_PRIORITY);
				sort.start();
				try {
					sort.join();
				} catch (Exception e) {
					e.printStackTrace();
				}

				long quickSortEnd = System.currentTimeMillis();

				System.out.println("Quick sort done in " + TimeUnit.MILLISECONDS.toSeconds(quickSortEnd - quickSortStart) + " seconds");


				//Testing binary search time
				for(int t = 0; t < 3; t++) {
					int target = new Random().nextInt(10000000);
					long manualStart = System.currentTimeMillis();

					boolean found = false;

					for(int i = 0; i < ARRAY.length; i++) {
						if(ARRAY[i] == target) {
							found = true;
						}
					}

					long manualEnd = System.currentTimeMillis();
					long binarySearchStart = System.currentTimeMillis();

					boolean binFound = (BinarySearch.containsValue(ARRAY, target) != -1);

					long binarySearchEnd = System.currentTimeMillis();

					long binTime = binarySearchEnd - binarySearchStart;
					long manTime = manualEnd - manualStart;

					System.out.println("The binary search was " + (manTime - binTime) + "ms faster!");
				}
				long testEnd = System.currentTimeMillis();

				System.out.println("Bench marking completed... Total run time: " + TimeUnit.MILLISECONDS.toSeconds(testEnd - testStart) + " seconds");
				
				int prev = -1;
				for(int i : ARRAY) {
					if(prev == -1) {
						prev = i;
					} else {
						if(i < prev) {
							System.out.println("Not sorted!");
							break;
						}
					}
				}
				System.out.println("No errors!");
				System.exit(0);
			}
		};
		main.start();
	}

	public static int worker_id = 0;
	public static Thread startWorker() {
		int[] array = new int[size / 7];
		worker_id+= 1;
		Thread worker = new Thread("Worker: " + worker_id) {
			@Override
			public void run() {
				long tstart = System.currentTimeMillis();
				for(int i = 0; i < size / 7; i+=1) {
					array[i] = random(1000000);
				}
				long tend = System.currentTimeMillis();
				System.out.println(this.getName() + " finished in: " + 
						TimeUnit.MILLISECONDS.toSeconds(tend - tstart) + " seconds!");
				insertIntoMasterArray(array);
			}
		};
		worker.setPriority(Thread.MAX_PRIORITY);
		worker.start();
		return worker;
	}

	public synchronized static void insertIntoMasterArray(int[] array) {
		for(int i : array) {
			ARRAY[currentPos] = i;
			currentPos++;
			totalValues++;
		}
	}

	private static long state = 0xCAFEBABE; // initial non-zero value.

	public static final int random(int n) {
		if (n<0) throw new IllegalArgumentException();
		long result=((nextLong()>>>32)*n)>>32;
				return (int) result;
	}

	public static final long nextLong() {
		long a=state;
		state = xorShift64(a);
		return a;
	}

	public static final long xorShift64(long a) {
		a ^= (a << 21);
		a ^= (a >>> 35);
		a ^= (a << 4);
		return a;
	}
}