import java.util.Random;


public class BubbleSort {
	
	public enum SortType {
		HIGHEST_TO_LOWEST, LOWEST_TO_HIGHEST;
	}

	/**
	 * A simple test that generates a random array with values ranging from 0-199
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		int[] array = new int[1000];
		
		for(int i = 0; i < 1000; i++) {
			array[i] = new Random().nextInt(200);
		}
		
		outputArray(array);
		System.out.println("");
		
		array = sort(array, SortType.LOWEST_TO_HIGHEST);
		outputArray(array);
	}
	
	/**
	 * A simple bubble sort algorythm.
	 * 
	 * @param array
	 * @param sortType
	 * 
	 * @return Sorted int[]
	 */
	public static int[] sort(int[] array, SortType sortType) {
		int index = array.length - 1;
		boolean exchanged = false;
		do {
			exchanged = false;
			
			for(int i = 0; i < index; i++) {
				boolean swap = false;
				if(sortType == SortType.HIGHEST_TO_LOWEST) {
					swap = array[i] < array[i + 1];
				} else {
					swap = array[i] > array[i + 1];
				}
				
				if(swap) {
					exchanged = true;
					int temp = array[i];
					array[i] = array[i + 1];
					array[i + 1] = temp;
				}
			}
			index--;
		} while(exchanged);
		return array;
	}
	
	/**
	 * Output the array to the console seperated with commas
	 * @param array
	 */
	public static void outputArray(int[] array) {
		for(int i = 0; i < array.length; i++) {
			System.out.print(array[i] + ", ");
		}
	}
}