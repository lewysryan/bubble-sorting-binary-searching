import static org.junit.Assert.*;

import java.util.Random;

import org.junit.Test;

public class BubbleSortTestSuite {

	@Test
	public void testLowestToHighestSort() {
		for(int t = 0; t < 1000; t++) {
			int[] array = new int[1000];

			for(int i = 0; i < 1000; i++) {
				array[i] = new Random().nextInt(200);
			}

			array = BubbleSort.sort(array, BubbleSort.SortType.LOWEST_TO_HIGHEST);

			boolean isOutOfOrder = false;
			int previous = Integer.MIN_VALUE;
			for(int i : array) {
				if(previous == Integer.MIN_VALUE) {
					previous = i;
				} else if(previous > i) {
					isOutOfOrder = true;
					break;
				}
				previous = i;
			}

			assertEquals(false, isOutOfOrder);
		}
	}

	@Test
	public void testHighestToLowest() {
		for(int t = 0; t < 1000; t++) {
			int[] array = new int[1000];

			for(int i = 0; i < 1000; i++) {
				array[i] = new Random().nextInt(200);
			}

			array = BubbleSort.sort(array, BubbleSort.SortType.HIGHEST_TO_LOWEST);

			boolean isOutOfOrder = false;
			int previous = Integer.MAX_VALUE;
			for(int i : array) {
				if(previous == Integer.MAX_VALUE) {
					previous = i;
				} else if(previous < i) {
					isOutOfOrder = true;
					break;
				}
				previous = i;
			}

			assertEquals(false, isOutOfOrder);
		}
	}
}
