import java.util.Random;

public class BinarySearch {

	public static void main(String[] args) {
		int[] array = new int[100];
		
		Random r = new Random();
		for(int i = 0; i < 100; i++) {
			array[i] = r.nextInt(5000);
		}
		
		array = BubbleSort.sort(array, BubbleSort.SortType.LOWEST_TO_HIGHEST);
		
		System.out.println("Contains 109?: " + (containsValue(array, 109) != -1));
		System.out.println("Contains 255?: " + (containsValue(array, 255) != -1));
		System.out.println("Contains 400?: " + (containsValue(array, 400) != -1));
		System.out.println("Contains 93?: " + (containsValue(array, 93) != -1));
		System.out.println("Contains 15?: " + (containsValue(array, 15) != -1));
		
		BubbleSort.outputArray(array);
	}
	
	/**
	 * A simple binary search algorithm
	 * 
	 * @param array
	 * @param target
	 * @return -1 if not found. Otherwise the targets position in the array
	 */
	public static int containsValue(int[] array, int target) {
		int upper = array.length - 1;
		int lower = 0;
		
		while(upper >= lower) {
			int midPoint = (upper + lower) / 2;
			if(array[midPoint] < target) {
				lower = midPoint + 1;
			} else if(array[midPoint] == target) {
				return midPoint;
			} else {
				upper = midPoint - 1;
			}
		}
		return -1;
	}
}
