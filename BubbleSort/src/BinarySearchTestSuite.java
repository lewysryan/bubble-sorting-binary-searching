import static org.junit.Assert.*;

import java.util.Random;

import org.junit.Test;


public class BinarySearchTestSuite {


	/**
	 * Runs the binary search 5000 times. Will first manually look through.
	 * Then will make sure the binary search produces the same result
	 */
	@Test
	public void testContains() {
		for(int t = 0; t < 5000; t++) {
			int[] array = new int[1000];

			for(int i = 0; i < 1000; i++) {
				array[i] = new Random().nextInt(1000);
			}
			
			array = BubbleSort.sort(array, BubbleSort.SortType.LOWEST_TO_HIGHEST);

			int target = new Random().nextInt(1000);
			
			boolean expected = false;
			for(int elm : array) {
				if(elm == target) {
					expected = true;
				}
			}
			
			boolean actual = BinarySearch.containsValue(array, target) != -1;
			
			assertEquals(expected, actual);
		}
	}
}